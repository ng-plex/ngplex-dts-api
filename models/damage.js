'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DamageSchema = new Schema({
  title: String,
  description: String,
  status: {
    type: String,
    enum: ['RECEIVED', 'IN_PROCESS', 'POSTPONED', 'COMPLETE'],
    default: 'RECEIVED'
  },
  damage_type: {
    type: Schema.ObjectId,
    ref: 'DamageType'
  },
  address: {
    line1: String,
    city: String,
    country: String,
    zipcode: Number
  },
  coords: {
    lat: Number,
    lng: Number
  },
  photoURL: String,
  user: {
    type: String,
    ref: 'User'
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('Damage', DamageSchema);