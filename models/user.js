'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    username: String,
    email: String,
    password: String,
    image: String,
    role: {
        type: String,
        enum: ['ROLE_ADMIN', 'ROLE_USER'],
        default: 'ROLE_USER'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);