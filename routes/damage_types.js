'use strict'

const express = require('express');
const DamageTypesController = require('../controllers/damage_types');

let api = express.Router();
let auth_middleware = require('../middlewares/auth'); 

api.get('/damage_types', auth_middleware.isAuth, DamageTypesController.getAllDamageTypes);
api.get('/damage_type/:code', auth_middleware.isAuth, DamageTypesController.getDamageTypeByCode);

module.exports = api;