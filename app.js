'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const methodoverride = require('method-override');
const morgan = require('morgan');

let app = express();

// Routes
let user_routes = require('./routes/user');
let damage_routes = require('./routes/damage');
let damage_types_routes = require('./routes/damage_types');

/*
 * Middlewares
 * 
 */

// Body Parser
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(methodoverride());
app.use(morgan('dev'));

// Removes the X-Powered-By Header
app.disable('x-powered-by');

//CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Base Routes
app.use('/api', user_routes);
app.use('/api', damage_routes);
app.use('/api', damage_types_routes);

module.exports = app;