'use strict';

// Modules load
const bcrypt = require('bcrypt-nodejs');
const fs = require('fs');
const path = require('path');

// Models
let DamageType = require('../models/damage_type');

// JWT Services
const jwtServices = require('../services/jwt');

/**
 * Controller Methods
 */

// Get All Damage Types
function getAllDamageTypes(req, res) {
  DamageType.find().exec((err, types) => {
    if (err) {
      res.status(500).send(`An error was found trying to get the Damage Types from the Server`);
    } else {
      if (!types) {
        res.status(404).send({
          message: `No Damage Types were found`
        });
      } else {
        res.status(200).send({
          damageTypes: types
        });
      }
    }
  })
}

// Get a DamageType by its code
function getDamageTypeByCode(req, res) {
  let damageTypeCode = req.params.code;
  if (!damageTypeCode) {
    res.status(404).send({
      message: 'The Damage Type Code must be provided'
    });
  }
  DamageType.find({code: damageTypeCode}).exec((error, damageType) => {
    if (error) {
      res.status(500).send({
        message: `Error trying to get the specified Damage Type`
      });
    } else {
      if (!damageType) {
        res.status(404).send({
          message: `No Damage Type was found with that code`
        });
      } else {
        res.status(200).send({
          damageType: damageType
        })
      }
    }
  });
}

module.exports = {
  getAllDamageTypes,
  getDamageTypeByCode
}