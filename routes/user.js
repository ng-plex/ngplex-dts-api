'use strict'

const express = require('express');
const UserController = require('../controllers/user');

let api = express.Router();
let auth_middleware = require('../middlewares/auth'); 

let multipart = require('connect-multiparty');
// let md_upload = multipart({ uploadDir: './uploads/users' });

api.post('/signup', UserController.signup);
api.post('/signin', UserController.signin);
api.put('/update-user/:id', auth_middleware.isAuth, UserController.updateUser);

module.exports = api;