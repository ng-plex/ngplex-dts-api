'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const jwtSecret = require('../env/environment.dev').auth.jwt_secret;

module.exports.isAuth = function (req, res, next) {
    if (!req.headers.authorization) {
        res.status(403).send({
            message: `You are not authorized, the Authorization header is not present in the Request`
        });
    }

    // TODO: 'Try with Bearer tokens'
    let token = req.headers.authorization.replace(/['"]+/g);
    let payload;

    try {
        payload = jwt.decode(token, jwtSecret);
        if (payload.exp <= moment().unix()) {
            return res.status(401).send({
                message: `Your token has expired, please generate a new one`
            })
        }
    } catch (error) {
        return res.status(401).send({
            message: `Invalid Token`
        })
    }
    // Get the User Payload to every request
    req.user = payload;
    
    next();
}