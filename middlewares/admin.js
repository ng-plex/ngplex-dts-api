'use strict';

/*
 * Middleware - IsAdmin in order to evaluate if the User has ROLE_ADMIN
 */
module.exports.isAdmin = function (req, res, next) {
    if (req.user.role != 'ROLE_ADMIN') {
        res.status(200).send({
            message: 'You have not access to this section because of your current ROLE'
        });
    }
    next();
}