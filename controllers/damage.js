'use strict';

// Modules load
const bcrypt = require('bcrypt-nodejs');
const fs = require('fs');
const path = require('path');

// Models
let User = require('../models/user');
let Damage = require('../models/damage');

// JWT Services
const jwtServices = require('../services/jwt');

/**
 * Controller Methods
 */

// Get All Damages
function getAllDamages(req, res) {
  Damage.find({})
    .populate({
      path: 'damage_type'
    })
    .populate({
      path: 'user'
    })
    .exec((err, damages) => {
      if (err) {
        res.status(500).send({
          message: 'You cannot get the Damage because an error occurrs'
        });
      } else {
        if (!damages) {
          res.status(404).send({
            message: 'There are no Damages'
          });
        } else {
          res.status(200).send({
            damages
          });
        }
      }
    });
}

// Get a Damage
function getDamage(req, res) {
  let damageID = req.params.id;
  if (!damageID) {
    res.status(400).send({
      message: `The Damage Identifier must be sent`
    });
  }

  Damage.findById(damageID).exec((err, damageResult) => {
    if (err) {
      res.status(500).send({
        message: `You cannot get the Damage because an error occurrs`
      })
    } else {
      if (!damageResult) {
        res.status(404).send({
          message: `No Damage was found with the supplied identifier`
        });
      } else {
        res.status(200).send({
          damage: damageResult
        })
      }
    }
  })

}

// Save a new Damage
function saveDamage(req, res) {
  let damage = new Damage();
  let bodyParams = req.body;

  if (bodyParams.title && bodyParams.description && bodyParams.lat && bodyParams.lng) {
    damage.title = bodyParams.title;
    damage.description = bodyParams.description;
    damage.damage_type = bodyParams.damage_type;
    damage.status = 'RECEIVED';
    damage.address.line1 = bodyParams.address_line1;
    damage.address.city = bodyParams.address_city;
    damage.address.country = bodyParams.address_country;
    damage.address.zipcode = bodyParams.address_zipcode;
    damage.coords.lat = bodyParams.lat;
    damage.coords.lng = bodyParams.lng;
    damage.photoURL = bodyParams.photoURL;
    damage.user = bodyParams.user;

    damage.save((err, savedDamage) => {

      if (err) {
        res.status(500).send({
          message: 'You cannot save the Damage because an error occurrs'
        });
      } else {
        if (!savedDamage) {
          res.status(404).send({
            message: 'You cannot save the Damage'
          });
        } else {
          res.status(200).send({
            animal: savedDamage
          });
        }
      }
    });

  } else {
    res.status(200).send({
      message: 'There are one or more required fields'
    });
  }

}

module.exports = {
  getAllDamages,
  getDamage,
  saveDamage
}