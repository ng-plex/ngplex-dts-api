'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  title: String,
  description: String,
  from: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  to: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  damage: {
    type: Schema.Types.ObjectId,
    ref: 'Damage'
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('Message', MessageSchema);