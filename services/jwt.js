'use strict';

const jwt = require('jwt-simple');
const moment = require('moment');
const jwtSecret = require('../env/environment.dev').auth.jwt_secret;

module.exports.createToken = function(user) {
    let payload = {
        sub: user._id,
        username: user.username,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment.unix(),
        exp: moment().add(30, 'days').unix()
    };

    return jwt.encode(payload, jwtSecret);
};