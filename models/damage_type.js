'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DamageTypeSchema = new Schema({
  code: String,
  type: String
});

module.exports = mongoose.model('DamageType', DamageTypeSchema, 'damage_types');