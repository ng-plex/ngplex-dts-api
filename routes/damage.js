'use strict'

const express = require('express');
const DamageController = require('../controllers/damage');

let api = express.Router();
let auth_middleware = require('../middlewares/auth');
let admin_middleware = require('../middlewares/admin');

let multipart = require('connect-multiparty');
// let md_upload = multipart({ uploadDir: './uploads/damages' });

api.get('/damages', auth_middleware.isAuth, DamageController.getAllDamages);
api.post('/damages', auth_middleware.isAuth, DamageController.saveDamage);
api.get('/damage/:id', auth_middleware.isAuth, DamageController.getDamage);
// api.put('/damage/:id', auth_middleware.isAuth, admin_middleware.isAdmin, DamageController.getAllDamages);
// api.delete('/damage/:id', auth_middleware.isAuth, admin_middleware.isAdmin, DamageController.getAllDamages);

module.exports = api;