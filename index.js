'use strict';

const mongoose = require('mongoose');

const app = require('./app');

const environment = require('./env/environment.dev');

const port = process.env.PORT || environment.port || 3443;

mongoose.Promise = global.Promise;

const MONGODB_URL =
    `mongodb://${environment.database.user}:${environment.database.password}@${environment.database.host}/${environment.database.name}`

mongoose.connect(MONGODB_URL).then(() => {
    console.log(`Connection Successfull to the MongoDB Database`);
    app.listen(port, () => {
        console.log(`Server running successfully on port ${port}`);
    });
}).catch((err) => {
    console.error(err);
});