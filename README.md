# NGPlex.IO - Damage Tracking System RESTfull API

RESTfull API for the Front-End Damage Tracking System **Angular 2+** application (ADMINISTRATION APP) and the **Ionic 3+** application (MOBILE APP).

It uses **Firebase Real-time Database** but it will be migrated to **MongoDB** NoSQL Database.
