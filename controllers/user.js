'use strict';

// Modules load
const bcrypt = require('bcrypt-nodejs');
const fs = require('fs');
const path = require('path');

// Models
let User = require('../models/user');

// JWT Services
const jwtServices = require('../services/jwt');

/**
 * Controller Methods
 */

// SignUp (Register)
function signup(req, res) {
  let user = new User();
  let bodyParams = req.body;

  if (bodyParams.password &&
    bodyParams.username &&
    bodyParams.email) {
    user.username = bodyParams.username;
    user.email = bodyParams.email;
    user.role = 'ROLE_USER';
    user.image = null;

    User.findOne({
      email: user.email.toLowerCase()
    }, (err, existingUser) => {
      if (err) {
        res.status(500).send({
          message: `Error trying to connect to the server`
        });
      } else {
        if (!existingUser) {
          // Encrypt the incoming password
          bcrypt.hash(bodyParams.password, null, null, (err, passwordHash) => {
            user.password = passwordHash;
            user.save((err, savedUser) => {
              if (err) {
                res.status(500).send({
                  message: `Error saving user in the Server`
                });
              } else {
                if (!savedUser) {
                  res.status(404).send({
                    message: `Cannot save the User`
                  });
                } else {
                  res.status(200).send({
                    user: savedUser
                  })
                }
              }
            })
          });
        }
      };
    })
  }
}

// SignIn (Login)
function signin(req, res) {
  let bodyParams = req.body;
  let email = bodyParams.email;
  let password = bodyParams.password;

  User.findOne({
    email: email.toLowerCase()
  }, (err, existingUser) => {
    if (err) {
      res.status(500).send({
        message: `Error trying to get the User from Server`
      });
    } else {
      if (existingUser) {
        bcrypt.compare(password, existingUser.password, (err, checkPasswordSuccess) => {
          if (checkPasswordSuccess) {
            if (bodyParams.get_token) {
              // Return the token
              res.status(200).send({
                token: jwtServices.createToken(existingUser)
              });
            } else {
              res.status(200).send({
                user: existingUser
              });
            }
          } else {
            res.status(403).send({
              message: `The password is incorrect`
            });
          }
        });

      } else {
        res.status(404).send({
          message: `The user cannot access`
        });
      }
    }
  });
}

// Update User
function updateUser(req, res) {
  let userId = req.params.id;
  let userUpdatedContent = req.body;

  // Remove the password from the User response
  delete userUpdatedContent.password;

  // Verify the uid equality
  if (userId != req.user.sub) {
    return res.status(500).send({
      message: 'You have not permissions to change the User'
    });
  }

  User.findByIdAndUpdate(userId, userUpdatedContent, {
    new: true
  }, (err, updatedUser) => {
    if (err) {
      res.status(500).send({
        message: 'Error trying to update the user in the Server'
      });
    } else {
      if (!updatedUser) {
        res.status(404).send({
          message: 'The user cannot be updated'
        });
      } else {
        res.status(200).send({
          user: updatedUser
        });
      }
    }
  });
}

// Upload User Image Avatar
function uploadUserImage(req, res) {
  // TODO: To be implemented
}

// Get User Image Avatar
function getUserImage(req, res) {
  // TODO: To be implemented
}

// Exports all Controller Methods
module.exports = {
  signup,
  signin,
  updateUser
}